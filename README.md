# ibacop2018 README

Our Instance Based Configured Portfolios follow two different strategies.
IBaCoP is configured a priori following a Pareto efficiency approach to select a sub-set of planners (baseline strategy), 
which receive the same execution time for all
planning problems. 
On the contrary, IBaCoP2 decides for each problem the sub-set of planners to use. 
Such decisions are based on predictive models learnt also with training instances gathered from previous executions of the base planners. Both portfolios compete in the sequential
satisficing, agile and multi-core tracks.
